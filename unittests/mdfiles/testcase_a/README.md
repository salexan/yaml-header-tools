---
responsible:
    - Anton Beta
    - Caren Delta
description: 	Alternating time and frequency sweeps of bits
    and bytes which does not make sense.

data:
- filename:	"*.xlsx"
  description:  unprocessed output of some random software
- filename: 	time-sweep*.dat
  description: 	time sweep data copied from .xlsx to separate
  		textfiles for easier analysis
- filename:	freq-sweep*.dat
  description:	frequency sweep data copied from .xlsx to separate
  		textfiles for easier analysis

empty_field:

tags:
- unit test
- test data
- time sweep
- frequency sweep
...
