# Tests for yaml_header_tools

from os.path import join, dirname
from yaml_header_tools import (get_header_from_file, add_header_to_file, clean_header,
                               save_header_to_file, kw_present,
                               MetadataFileMissing, NoValidHeader, ParseErrorsInHeader)
from pytest import raises

MDFILES = join(dirname(__file__), "mdfiles")

def test_standard_header():
    # Test loading from directory:
    d = get_header_from_file(join(MDFILES, "testcase_a"))
    assert len(d["responsible"]) == 2
    assert d["description"][0].startswith("Alternating")
    assert len(d["data"]) == 3
    assert d["tags"][0] == "unit test"
    assert d["tags"][-1] == "frequency sweep"
    assert d["empty_field"] == [""]

    # Test loading from directory, without cleaning the header:
    d = get_header_from_file(join(MDFILES, "testcase_a"), False)
    assert len(d["responsible"]) == 2
    assert d["description"].startswith("Alternating")
    assert len(d["data"]) == 3
    assert d["tags"][0] == "unit test"
    assert d["tags"][-1] == "frequency sweep"
    assert d["empty_field"] == ""

    # Test loading file directly:
    d = get_header_from_file(join(MDFILES, "testcase_a", "README.md"), True)
    assert len(d["responsible"]) == 2
    assert len(d["data"]) == 3

def test_loading_order():
    # Test correct loading order of readme files in folder:
    d = get_header_from_file(join(MDFILES, "testcase_b"), False)
    assert len(d["responsible"]) == 2
    d = get_header_from_file(join(MDFILES, "testcase_b", "readme.md"), False)
    assert len(d["responsible"]) == 1


def test_invalid():
    with raises(NoValidHeader):
        d = get_header_from_file(join(MDFILES, "testcase_broken_1"))
    with raises(MetadataFileMissing):
        d = get_header_from_file(join(MDFILES, "testcase_broken_2"))
    with raises(NoValidHeader):
        d = get_header_from_file(join(MDFILES, "testcase_broken_3"))
    with raises(ParseErrorsInHeader):
        d = get_header_from_file(join(MDFILES, "testcase_broken_4"))
    with raises(NoValidHeader):
        d = get_header_from_file(join(MDFILES, "testcase_broken_5"))

def test_clean_header():
    h = {
        "test": None,
        "test2": "null"}
    cleaned_h = clean_header(h)
    assert cleaned_h["test"][0] == ""
    assert cleaned_h["test2"][0] == ""

def test_write_header(tmp_path):
    with raises(MetadataFileMissing):
        # for non-existing file (without the create_if_not_exists option):
        f = tmp_path / "test_missing.md"
        add_header_to_file(f, {"test": "text"}, False)
    
    # for non-existing file:
    f = tmp_path / "test.md"
    add_header_to_file(f, {"test": "text"}, True)
    # check validity:
    d = get_header_from_file(f)
    assert len(d) == 1
    assert d["test"][0] == "text"

    # currently saving integers is impossible:
    with raises(TypeError):
        save_header_to_file(f, {"another_test": 4})

    save_header_to_file(f, {"another_test": "4"})
    d = get_header_from_file(f)
    assert len(d) == 1
    assert d["another_test"][0] == "4"

    # for existing file:
    f = tmp_path / "test_second.md"
    f.write_text("Existing text")
    add_header_to_file(f, {"test": "text"})
    # check validity:
    d, main_content = get_header_from_file(f, return_main_content=True)
    assert len(d) == 1
    assert d["test"][0] == "text"
    assert main_content[0] == "Existing text"

    save_header_to_file(f, {"another_test": "4"})
    d, main_content = get_header_from_file(f, return_main_content=True)
    assert len(d) == 1
    assert d["another_test"][0] == "4"
    assert main_content[0] == "Existing text"

    # try saving header to file without existing header
    f = tmp_path / "test_third.md"
    f.write_text("Existing text")
    with raises(NoValidHeader):
        save_header_to_file(f, {"another_test": "4"})

def test_kw_present():
    d = get_header_from_file(join(MDFILES, "testcase_a", "README.md"), True)
    assert kw_present(d, "responsible") is True
    assert kw_present(d, "data") is True
    assert kw_present(d, "test") is False
