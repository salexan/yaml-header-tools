# Changelog #

All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.1.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## [Unreleased] ##

### Added ###

### Changed ###

### Deprecated ###

### Removed ###

### Fixed ###


### Security ###

## [0.2.1] - 2022-09-22 ##

### Changed ###

- Made type hints backwards compatible to Python < 3.9

## [0.2.0] - 2021-11-23 ##

### Added ###

- New project folder structure and package configuration
- Code-coverage using pytest
- More unit tests for almost 100% code coverage

### Changed ###

- Added type annotations for all function arguments
